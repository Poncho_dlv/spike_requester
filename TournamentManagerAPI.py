#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from typing import List

import requests
from spike_settings.SpikeSettings import SpikeSettings

spike_logger = logging.getLogger("spike_logger")


class TournamentManagerAPI:
    def __init__(self):
        pass

    @staticmethod
    def create_tournament(tournament_name: str, tournament_format: str, platform: str, round_format: str, number_of_round: int, win_value: int,
                          draw_value: int, loss_value: int,
                          tie_breakers: List[str]):
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        body = {
            "name": tournament_name,
            "format": tournament_format,
            "platform": platform,
            "round_format": round_format,
            "number_of_round": number_of_round,
            "win_value": win_value,
            "draw_value": draw_value,
            "loss_value": loss_value,
            "tie_breakers": tie_breakers
        }
        return TournamentManagerAPI.post_request("/create_tournament", headers=headers, body=body)

    @staticmethod
    def delete_tournament(tournament_id: str):
        endpoint = "/tournaments/{}/delete".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.delete_request(endpoint, headers=headers)

    @staticmethod
    def start_tournament(tournament_id: str):
        endpoint = "/tournaments/{}/start".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers)

    @staticmethod
    def close_tournament(tournament_id: str):
        endpoint = "/tournaments/{}/close".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers)

    @staticmethod
    def get_tournament(tournament_id: str):
        endpoint = "/tournaments/{}".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request(endpoint, headers=headers)

    @staticmethod
    def get_active_tournaments():
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request("/tournaments/active_tournaments", headers=headers)

    @staticmethod
    def update_registered_leagues(tournament_id: str, leagues: List[dict]):
        endpoint = "/tournaments/{}/update_leagues".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        body = {
            "leagues": leagues
        }
        return TournamentManagerAPI.put_request(endpoint, headers=headers, body=body)

    @staticmethod
    def create_signup(tournament_id: str):
        endpoint = "/tournaments/{}/create_sign_up".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.post_request(endpoint, headers=headers)

    @staticmethod
    def create_competitions(tournament_id: str, round_index: int):
        endpoint = "/tournaments/{}/rounds/{}/create_competitions".format(tournament_id, round_index)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.post_request(endpoint, headers=headers)

    @staticmethod
    def register_teams(tournament_id: str):
        endpoint = "/tournaments/{}/register_teams".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.post_request(endpoint, headers=headers)

    @staticmethod
    def register_team(tournament_id: str, team_name: str, team_id: int, platform_id: int, logo: str, race_id: int, coach_name: str, coach_id: int):
        endpoint = "/tournaments/{}/register_team".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        body = {
            "name": team_name,
            "id": team_id,
            "platform_id": platform_id,
            "logo": logo,
            "race_id": race_id,
            "coach_name": coach_name,
            "coach_id": coach_id,
        }
        return TournamentManagerAPI.post_request(endpoint, headers=headers, body=body)

    @staticmethod
    def unregister_team(team_id: str):
        endpoint = "/teams/{}/unregister".format(team_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers)

    @staticmethod
    def get_registered_teams(tournament_id: str, active_only: bool = True):
        endpoint = "/tournaments/{}/teams".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        params = {"active_only": active_only}
        return TournamentManagerAPI.get_request(endpoint, headers=headers, params=params)

    @staticmethod
    def get_team(team_id: str):
        endpoint = "/teams/{}".format(team_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request(endpoint, headers=headers)

    @staticmethod
    def get_contests(tournament_id: str, round_index: int, allow_draw: bool = False, public_only: bool = False):
        endpoint = "/tournaments/{}/rounds/{}/contests".format(tournament_id, round_index)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        params = {
            "allow_draw": allow_draw,
            "public_only": public_only
        }
        return TournamentManagerAPI.get_request(endpoint, headers=headers, params=params)

    @staticmethod
    def get_contest(contest_id: str):
        endpoint = "/contests/{}".format(contest_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request(endpoint, headers=headers)

    @staticmethod
    def get_current_round(tournament_id: str):
        endpoint = "/tournaments/{}/current_round".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request(endpoint, headers=headers)

    @staticmethod
    def update_contest(contest_id: str, contest_data):
        endpoint = "/contests/{}/update".format(contest_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers, body=contest_data)

    @staticmethod
    def replace_team(contest_id: str, team_id: str, new_team_id: str):
        endpoint = "/contests/{}/teams/{}/replace".format(contest_id, team_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        params = {
            "new_team_id": new_team_id
        }
        return TournamentManagerAPI.put_request(endpoint, headers=headers, params=params)

    @staticmethod
    def publish_round(tournament_id: str, round_index):
        endpoint = "/tournaments/{}/rounds/{}/publish".format(tournament_id, round_index)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers)

    @staticmethod
    def unpublish_round(tournament_id: str, round_index):
        endpoint = "/tournaments/{}/rounds/{}/unpublish".format(tournament_id, round_index)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.put_request(endpoint, headers=headers)

    @staticmethod
    def get_standing(tournament_id: str):
        endpoint = "/tournaments/{}/standing".format(tournament_id)
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return TournamentManagerAPI.get_request(endpoint, headers=headers)

    @staticmethod
    def post_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
        url = SpikeSettings.get_tournament_manager_base_url() + endpoint
        req = requests.post(url, params=params, headers=headers, json=body, timeout=45)
        if req.status_code == 200:
            ret = req.json()
        else:
            ret = None
            spike_logger.error("post_request error :: url: {} :: status_code {}".format(url, req.status_code))
        return ret

    @staticmethod
    def put_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
        url = SpikeSettings.get_tournament_manager_base_url() + endpoint
        req = requests.put(url, params=params, headers=headers, json=body, timeout=45)
        if req.status_code == 200:
            ret = req.json()
        else:
            ret = None
            spike_logger.error("put_request error :: url: {} :: status_code {}".format(url, req.status_code))
        return ret

    @staticmethod
    def get_request(endpoint: str, params: dict = None, headers: dict = None):
        url = SpikeSettings.get_tournament_manager_base_url() + endpoint
        req = requests.get(url, params=params, headers=headers, timeout=45)
        if req.status_code == 200:
            ret = req.json()
        else:
            ret = None
            spike_logger.error("get_request error :: url: {} :: status_code {}".format(url, req.status_code))
        return ret

    @staticmethod
    def delete_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
        url = SpikeSettings.get_tournament_manager_base_url() + endpoint
        req = requests.delete(url, params=params, headers=headers, json=body, timeout=45)
        if req.status_code == 200:
            ret = req.json()
        else:
            ret = None
            spike_logger.error("delete_request error :: url: {} :: status_code {}".format(url, req.status_code))
        return ret
