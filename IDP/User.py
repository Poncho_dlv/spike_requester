#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List

from spike_requester.SpikeAPI import get_request
from spike_settings.SpikeSettings import SpikeSettings


class User:
    def __init__(self):
        self.base_url = SpikeSettings.get_idp_api_base_url()

    def verify_token(self, user_id: str, token: str, scope: str):
        endpoint = f"{self.base_url}/users/{user_id}/verify_token"
        headers = {
            "token": token,
            "scope": scope
        }
        return get_request(endpoint, headers=headers)
