#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List

from spike_requester import send_request
from spike_settings.SpikeSettings import SpikeSettings


class App:
    def __init__(self):
        self.base_url = SpikeSettings.get_idp_api_base_url()

    def create_app(self, app_name: str, token: str):
        url = f"{self.base_url}/apps/create"
        return send_request("post", url, headers={"token": token}, body={"name": app_name})

    def remove_app(self, application_id: str, token: str):
        url = f"{self.base_url}/apps/{application_id}/remove"
        return send_request("delete", url, headers={"token": token})

    def get_token(self, application_id: str,client_id: str, client_secret: str, token: str, scopes: List[str]):
        url = f"{self.base_url}/apps/{application_id}/token"
        headers = {
            "token": token
        }

        body = {
            "client_id": client_id,
            "client_secret": client_secret,
            "scopes": scopes
        }
        return send_request("post", url, headers=headers, body=body)

    def verify_token(self, application_id: str, token: str, scope: str):
        url = f"{self.base_url}/apps/{application_id}/verify_token"
        headers = {
            "token": token,
            "scope": scope
        }
        return send_request("get", url, headers=headers)
