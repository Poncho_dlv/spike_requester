#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List

from spike_requester import send_request
from spike_settings.SpikeSettings import SpikeSettings


class CyanideApi:
    def __init__(self):
        pass

    @staticmethod
    def get_coaches(league_name: str, competition_name: str = None, limit: int = 500, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("coaches")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "limit": limit
        }
        if competition_name is not None:
            params["competition"] = competition_name
        return send_request("get", url, params=params)

    @staticmethod
    def get_matches(league_list: List[str], limit: int, platform: str = "pc", start: str = None, end: str = None, id_only: bool = False,
                    order: str = "finished"):
        if len(league_list) == 0:
            return None

        url = SpikeSettings.get_cyanide_bb2_base_url().format("matches")
        leagues = ",".join(league_list)
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": leagues,
            "platform": platform,
            "limit": limit,
            "order": order
        }
        if id_only is True:
            params["id_only"] = 1

        if end is not None:
            params["end"] = end
        if start is not None:
            params["start"] = start
        return send_request("get", url, params=params)

    @staticmethod
    def get_teams(league_name: str, competition_name: str = None, limit: int = 500, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("teams")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "limit": limit
        }
        if competition_name is not None:
            params["competition"] = competition_name
        return send_request("get", url, params=params)

    @staticmethod
    def get_ladder(league_name: str, competition_name: str, ladder_size: int = 100, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("ladder")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "ladder_size": ladder_size
        }
        if competition_name is not None:
            params["competition"] = competition_name
        return send_request("get", url, params=params)

    @staticmethod
    def get_match(match_uuid: str):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("match")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "match_id": match_uuid
        }
        return send_request("get", url, params=params)

    @staticmethod
    def get_hall_of_fame(league_name: str, competition_name: str, limit: int = 100, exact: int = 1, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("halloffame")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "competition": competition_name,
            "platform": platform,
            "limit": limit,
            "exact": exact
        }
        return send_request("get", url, params=params)

    @staticmethod
    def get_contests(league_name: str, competition_name: str = None, limit: int = 100, status: str = "scheduled", exact: int = 1,
                     round_number: int = None, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("contests")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "status": status,
            "limit": limit,
            "exact": exact
        }
        if competition_name is not None:
            params["competition"] = competition_name
        if round_number is not None:
            params["round"] = round_number
        return send_request("get", url, params=params)

    @staticmethod
    def get_competitions(league_name: str, limit: int = 100, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("competitions")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "limit": limit,
            "exact": 1
        }
        ret = send_request("get", url, params=params)
        if not ret or len(ret.get("competitions", [])) == 0:
            params["exact"] = 0
            ret2 = send_request("get", url, params=params)
            if not ret2 or len(ret2.get("competitions", [])) == 0:
                ret = {"competitions": []}
            else:
                ret = ret2
        return ret

    @staticmethod
    def get_team(team_name: str = None, platform: str = "pc", team_id: int = None):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("team")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "platform": platform,
            "order": "CreationDate",
            "name": team_name if team_name is not None else team_id
        }
        if team_name is not None:
            params["name"] = team_name
        elif team_id is not None:
            params["team"] = team_id
        else:
            return None
        return send_request("get", url, params=params)

    @staticmethod
    def get_league(league_name: str = "", platform: str = "pc", league_id: int = None):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("league")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "platform": platform
        }
        if league_id is not None:
            params["id"] = league_id
        else:
            params["league"] = league_name
        return send_request("get", url, params=params)

    @staticmethod
    def get_leagues(league_name: str, limit: int = 100, nb_team_min: int = 1, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("leagues")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "league": league_name,
            "platform": platform,
            "limit": limit,
            "teams": nb_team_min
        }
        return send_request("get", url, params=params)

    @staticmethod
    def get_player(player_id: int = None, player_name: str = None, platform: str = "pc"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("player")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "platform": platform
        }
        if player_id is not None:
            params["player"] = player_id
        elif player_name is not None:
            params["name"] = player_name
        else:
            return None
        return send_request("get", url, params=params)

    @staticmethod
    def get_team_matches(team_id: int, limit: int = 100, platform: str = "pc", start: str = None, end: str = None, order: str = "finished"):
        url = SpikeSettings.get_cyanide_bb2_base_url().format("teammatches")
        params = {
            "key": SpikeSettings.get_cyanide_key(),
            "team_id": team_id,
            "limit": limit,
            "platform": platform,
            "order": order
        }
        if start is not None:
            params["start"] = start
        if end is not None:
            params["end"] = end
        return send_request("get", url, params=params)
