#!/usr/bin/env python
# -*- coding: utf-8 -*-
from spike_requester.SpikeAPI import get_request
from spike_settings.SpikeSettings import SpikeSettings


class LeagueAPI:
    def __init__(self):
        pass

    @staticmethod
    def get_competitions(platform_id: int, league_id: int, status: int = None):
        endpoint = f"/platforms/{platform_id}/leagues/{league_id}/competitions"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        params = {}
        if status is not None:
            params["status"] = status
        return get_request(endpoint, params=params, headers=headers)

    @staticmethod
    def get_league(platform_id: int, league_id: int):
        endpoint = f"/platforms/{platform_id}/leagues/{league_id}"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

