#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List

from spike_requester.SpikeAPI import get_request, post_request
from spike_settings.SpikeSettings import SpikeSettings


class CompetitionAPI:
    def __init__(self):
        pass

    @staticmethod
    def get_competition(platform_id: int, competition_id: int):
        endpoint = f"/platforms/{platform_id}/competitions/{competition_id}"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_contests(platform_id: int, competition_id: int, status: int = None, round_index: int = None, started: int = 0, finished: int = 0):
        endpoint = f"/platforms/{platform_id}/competitions/{competition_id}/contests"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        params = {}
        if status is not None:
            params["status"] = status
        if round_index is not None:
            params["round_index"] = round_index
        if started is not None:
            params["started"] = started
        if finished is not None:
            params["finished"] = finished
        return get_request(endpoint, params=params, headers=headers)

    @staticmethod
    def get_standing(platform_id: int, competition_id: int):
        endpoint = f"/platforms/{platform_id}/competitions/{competition_id}/standing"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_bracket(platform_id: int, competition_id: int):
        endpoint = f"/platforms/{platform_id}/competitions/{competition_id}/bracket"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def add_competition(platform_id: int, competitions: List[dict]):
        endpoint = f"/platforms/{platform_id}/competitions/add"
        body = {'competitions_list': competitions}
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return post_request(endpoint, headers=headers, body=body)
