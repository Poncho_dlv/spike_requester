#!/usr/bin/env python
# -*- coding: utf-8 -*-
from spike_requester.SpikeAPI import get_request
from spike_settings.SpikeSettings import SpikeSettings


class CclAPI:
    def __init__(self):
        pass

    @staticmethod
    def get_ban_list(platform_id: int):
        endpoint = f"/platforms/{platform_id}/ccl/current_season/ban_list"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_current_season(platform_id: int):
        endpoint = f"/platforms/{platform_id}/ccl/current_season"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_seasons(platform_id: int):
        endpoint = f"/platforms/{platform_id}/ccl/seasons"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_ranking(platform_id: int, season_index: int, limit: int = 10):
        endpoint = f"/platforms/{platform_id}/ccl/seasons/{season_index}/ranking"
        params = {"limit": limit}
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)

    @staticmethod
    def get_bracket(platform_id: int, season_index: int, round_index: int = None):
        endpoint = f"/platforms/{platform_id}/ccl/seasons/{season_index}/playoffs_bracket"
        if round_index is not None:
            params = {"round_index": round_index}
        else:
            params = None
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)

    @staticmethod
    def get_predictions(platform_id: int):
        endpoint = f"/platforms/{platform_id}/ccl/current_season/predictions"
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, headers=headers)

    @staticmethod
    def get_top_race(platform_id: int, season_index: int, race: str = None, limit: int = 5):
        endpoint = f"/platforms/{platform_id}/ccl/seasons/{season_index}/top_races"
        params = {"limit": limit}
        if race is not None:
            params['race'] = race
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)

    @staticmethod
    def get_win_rate(coach_id: int, platform_id: int, race_id: int = None, season_number: int = None, top_race_limit: int = 0):
        endpoint = f"/platforms/{platform_id}/ccl/coaches/{coach_id}/win_rate"
        params = {
            "race_id": race_id,
            "season_number": season_number,
            "top_race_limit": top_race_limit
        }
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)
