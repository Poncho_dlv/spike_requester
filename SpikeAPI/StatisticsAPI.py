#!/usr/bin/env python
# -*- coding: utf-8 -*-
from spike_requester.SpikeAPI import get_request
from spike_settings.SpikeSettings import SpikeSettings


class StatisticsAPI:
    def __init__(self):
        pass

    @staticmethod
    def get_win_rate(coach_id: int, platform_id: int, race_id: int = None, league_name: str = None, top_race_limit: int = 0, top_league_limit: int = 0):
        endpoint = f"/platforms/{platform_id}/coaches/{coach_id}/win_rate"
        params = {
            "race_id": race_id,
            "league_name": league_name,
            "top_race_limit": top_race_limit,
            "top_league_limit": top_league_limit
        }
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)

    @staticmethod
    def get_versus(coach1_id: int, coach2_id: int, platform_id: int, competition_name: str = None,  league_name: str = None, display_match_list: bool = False):
        endpoint = f"/platforms/{platform_id}/coaches/{coach1_id}/vs/{coach2_id}"
        params = {
            "competition_name": competition_name,
            "league_name": league_name,
            "display_match_list": display_match_list
        }
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)

    @staticmethod
    def compute_dode(win: int = 0, draw: int = 0, loss: int = 0):
        endpoint = "/compute_rank"
        params = {
            "win": win,
            "draw": draw,
            "loss": loss,
        }
        headers = {"API_KEY": SpikeSettings.get_spike_api_key()}
        return get_request(endpoint, params, headers)