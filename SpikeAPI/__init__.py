#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

import requests
from spike_settings.SpikeSettings import SpikeSettings

spike_logger = logging.getLogger("spike_logger")
spike_requester_logger = logging.getLogger("spike_requester")


def get_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
    url = SpikeSettings.get_spike_api_base_url() + endpoint
    req = requests.get(url, params=params, headers=headers, json=body, timeout=45)
    if req.status_code == 200:
        ret = req.json()
    else:
        ret = None
        spike_logger.error("get_request error :: url: {} :: status_code {}".format(url, req.status_code))
    return ret


def post_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
    url = SpikeSettings.get_tournament_manager_base_url() + endpoint
    req = requests.post(url, params=params, headers=headers, json=body, timeout=45)
    if req.status_code == 200:
        ret = req.json()
    else:
        ret = None
        spike_logger.error("post_request error :: url: {} :: status_code {}".format(url, req.status_code))
    return ret

def delete_request(endpoint: str, params: dict = None, headers: dict = None, body: dict = None):
    url = SpikeSettings.get_tournament_manager_base_url() + endpoint
    req = requests.delete(url, params=params, headers=headers, json=body, timeout=45)
    if req.status_code == 200:
        ret = req.json()
    else:
        ret = None
        spike_logger.error("post_request error :: url: {} :: status_code {}".format(url, req.status_code))
    return ret