#!/usr/bin/env python
# -*- coding: utf-8 -*-
from spike_database.Coaches import Coaches
from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_database.Matches import Matches
from spike_database.Players import Players
from spike_database.ResourcesRequest import ResourcesRequest
from spike_database.Teams import Teams
from spike_model.Match import Match
from spike_model.Team import Team
from spike_requester.CyanideApi import CyanideApi


class Utilities:

    def __init__(self):
        pass

    @staticmethod
    def get_match(match_uuid: str, raw_data: bool = False, force_refresh: bool = False):
        match = None
        matches_db = Matches()
        match_data = None

        if not force_refresh:
            try:
                match_data = matches_db.get_match_data(match_uuid)
            except Exception as e:
                print(f"Error in get_match: {e}")
                pass

        if match_data is None or match_data == "null":
            match_data = CyanideApi.get_match(match_uuid)
            if match_data is not None and match_data is not False and "match" in match_data.keys():
                match_data.pop("urls", None)
                match_data.pop("meta", None)
                matches_db.add_match(match_uuid, match_data)

        if match_data and "match" in match_data.keys():
            if raw_data:
                match = match_data
                match_model = Match(match_data)
                match_model.set_uuid(match_uuid)
                Utilities.update_data_from_match(match_model)
            else:
                match = Match(match_data)
                match.set_uuid(match_uuid)
                Utilities.update_data_from_match(match)
        return match

    @staticmethod
    def update_data_from_match(match_model):
        league_db = Leagues()
        competition_db = Competitions()
        coach_db = Coaches()

        league_db.add_or_update_league(match_model.get_league_name(), match_model.get_league_id(), match_model.get_platform_id())
        competition_db.add_or_update_competition(match_model.get_competition_name(), match_model.get_competition_id(), match_model.get_league_id(),
                                                 match_model.get_platform_id())
        home_coach = coach_db.get_coach_data(coach_id=match_model.get_coach_home().get_id(), platform_id=match_model.get_platform_id())
        if home_coach is None:
            coach_db.add_or_update_coach(match_model.get_coach_home().get_id(), match_model.get_coach_home().get_name(),
                                         match_model.get_platform_id())
        away_coach = coach_db.get_coach_data(coach_id=match_model.get_coach_away().get_id(), platform_id=match_model.get_platform_id())
        if away_coach is None:
            coach_db.add_or_update_coach(match_model.get_coach_away().get_id(), match_model.get_coach_away().get_name(),
                                         match_model.get_platform_id())

    @staticmethod
    def get_team(team_name: str = None, platform_id: int = 1, team_id: int = None, cached_data=False):
        player_db = Players()
        team_db = Teams()
        coach_db = Coaches()
        rsrc_db = ResourcesRequest()
        platform = rsrc_db.get_platform_label(platform_id)
        team = None

        if not cached_data:
            team_data = CyanideApi.get_team(team_name=team_name, team_id=team_id, platform=platform)
            if team_data and team_data is not False and team_data.get("roster"):
                team = Team(team_data)
                team.init_team_from_team_ws()
                team_db.add_or_update_team(team, platform_id)
                player_db.add_or_update_players(team.get_players(), platform_id, team.get_id(), True)
                if coach_db.get_coach_data(coach_id=team.get_coach().get_id(), platform_id=platform_id) is None:
                    coach_db.add_or_update_coach(team.get_coach().get_id(), team.get_coach().get_name(), platform_id)
        else:
            team_data = team_db.get_team(team_id, platform_id)
            if team_data is not None:
                team = Team(team_data)
                team.init_team_from_database()

        return team
