#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests


def send_request(method: str, url: str, params: dict = None, headers: dict = None, body: dict = None, timeout: int = 30):
    try:
        if method == "get":
            req = requests.get(url, params=params, headers=headers, timeout=timeout)
        elif method == "post":
            req = requests.post(url, params=params, headers=headers, json=body, timeout=timeout)
        elif method == "delete":
            req = requests.delete(url, params=params, headers=headers, json=body, timeout=timeout)
        elif method == "patch":
            req = requests.patch(url, params=params, headers=headers, json=body, timeout=timeout)
        elif method == "put":
            req = requests.put(url, params=params, headers=headers, json=body, timeout=timeout)
        else:
            print(f"{method} {url} invalid method")
            return None

        if req.status_code == 200:
            ret = req.json()
            print(f"{method} {url} {req.status_code}")
        else:
            ret = None
            print(f"{method} {url} {req.status_code}")
        return ret
    except Exception as e:
        print(f"{method} {url} {e}")
